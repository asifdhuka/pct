
public class StringFunctions {
	
	 public static void main(String args[]) {
	      String s = "Strings are immutable";
	      char result = s.charAt(8);
	      System.out.println(result);
	   }
	 public static void main(String args[]) {
	      String str1 = "Strings are immutable";
	      String str2 = new String("Strings are immutable");
	      String str3 = new String("Integers are not immutable");
	      
	      int result = str1.compareTo( str2 );
	      System.out.println(result);
	      
	      result = str2.compareTo( str3 );
	      System.out.println(result);
	   }
	 
	 public static void main(String args[]) {
	      String s = "Strings are immutable";
	      s = s.concat(" all the time");
	      System.out.println(s);
	   }
	 public static void main(String args[]) {
	      String str1 = "Not immutable";
	      String str2 = "Strings are immutable";
	      StringBuffer str3 = new StringBuffer( "Not immutable");

	      boolean  result = str1.contentEquals( str3 );
	      System.out.println(result);
		  
	      result = str2.contentEquals( str3 );
	      System.out.println(result);
	   }
	 
	 public static void main(String args[]) {
	      char[] Str1 = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
	      String Str2 = "";
	      Str2 = Str2.copyValueOf( Str1 );
	      System.out.println("Returned String: " + Str2);
	   }
	 
	 public static void main(String args[]) {
	      char[] Str1 = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
	      String Str2 = "";
	      Str2 = Str2.copyValueOf( Str1, 2, 6 );
	      System.out.println("Returned String: " + Str2);
	   }
	 public static void main(String args[]) {
	      String Str = new String("This is really not immutable!!");
	      boolean retVal;

	      retVal = Str.endsWith( "immutable!!" );
	      System.out.println("Returned Value = " + retVal );

	      retVal = Str.endsWith( "immu" );
	      System.out.println("Returned Value = " + retVal );
	   }
	 public static void main(String args[]) {
	      String Str1 = new String("This is really not immutable!!");
	      String Str2 = Str1;
	      String Str3 = new String("This is really not immutable!!");
	      boolean retVal;

	      retVal = Str1.equals( Str2 );
	      System.out.println("Returned Value = " + retVal );

	      retVal = Str1.equals( Str3 );
	      System.out.println("Returned Value = " + retVal );
	   }
	 public static void main(String args[]) {
	      String Str = new String("Welcome to Tutorialspoint.com");
	      System.out.print("Found Index :" );
	      System.out.println(Str.indexOf( 'o' ));
	   }
	 public static void main(String args[]) {
	      String Str = new String("Welcome to Tutorialspoint.com");
	      System.out.print("Found Index :" );
	      System.out.println(Str.indexOf( 'o', 5 ));
	   }
	 public static void main(String args[]) {
	      String Str = new String("Welcome to Tutorialspoint.com");
	      System.out.print("Found Last Index :" );
	      System.out.println(Str.lastIndexOf( 'o' ));
	   }
	  public static void main(String args[]) {
	      String Str1 = new String("Welcome to Tutorialspoint.com");
	      String Str2 = new String("Tutorials" );

	      System.out.print("String Length :" );
	      System.out.println(Str1.length());

	      System.out.print("String Length :" );
	      System.out.println(Str2.length());
	   }
	  public static void main(String args[]) {
	      String Str = new String("Welcome to Tutorialspoint.com");

	      System.out.print("Return Value :" );
	      System.out.println(Str.matches("(.*)Tutorials(.*)"));

	      System.out.print("Return Value :" );
	      System.out.println(Str.matches("Tutorials"));

	      System.out.print("Return Value :" );
	      System.out.println(Str.matches("Welcome(.*)"));
	   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :" );
		      System.out.println(Str.replace('o', 'T'));

		      System.out.print("Return Value :" );
		      System.out.println(Str.replace('l', 'D'));
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome-to-Tutorialspoint.com");
		      System.out.println("Return Value :" );      
		      
		      for (String retval: Str.split("-")) {
		         System.out.println(retval);
		      }
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :" );
		      System.out.println(Str.toCharArray() );
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :");
		      System.out.println(Str.toLowerCase());
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :" );
		      System.out.println(Str.substring(10) );

		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :");
		      System.out.println(Str.toLowerCase());
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :");
		      System.out.println(Str.toLowerCase());
		   }
	   public static void main(String args[]) {
		      String Str = new String("Welcome to Tutorialspoint.com");

		      System.out.print("Return Value :");
		      System.out.println(Str.toString());
		   }
	   public static void main(String args[]) {
		      String Str = new String("   Welcome to Tutorialspoint.com   ");

		      System.out.print("Return Value :" );
		      System.out.println(Str.trim() );
		   }
	   public static void main(String args[]) {
		      // char grade = args[0].charAt(0);
		      char grade = 'C';

		      switch(grade) {
		         case 'A' :
		            System.out.println("Excellent!"); 
		            break;
		         case 'B' :
		         case 'C' :
		            System.out.println("Well done");
		            break;
		         case 'D' :
		            System.out.println("You passed");
		         case 'F' :
		            System.out.println("Better try again");
		            break;
		         default :
		            System.out.println("Invalid grade");
		      }
		      System.out.println("Your grade is " + grade);
		   }
	   public static void main(String[] args) {
		      double[] myList = {1.9, 2.9, 3.4, 3.5};

		      // Print all the array elements
		      for (double element: myList) {
		         System.out.println(element);
		      }
		   }
	   public static void printArray(int[] array) {
		   for (int i = 0; i < array.length; i++) {
		      System.out.print(array[i] + " ");
		   }
		}
	   public static int[] reverse(int[] list) {
		   int[] result = new int[list.length];

		   for (int i = 0, j = result.length - 1; i < list.length; i++, j--) {
		      result[j] = list[i];
		   }
		   return result;
		}
	   public static void main(String args[]) {
		      // Instantiate a Date object
		      Date date = new Date();

		      // display time and date using toString()
		      System.out.println(date.toString());
		   }
	   public static void main(String args[]) {
		      Date dNow = new Date( );
		      SimpleDateFormat ft = 
		      new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

		      System.out.println("Current Date: " + ft.format(dNow));
		   }
	   public static void main(String args[]) {
		      SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd"); 
		      String input = args.length == 0 ? "1818-11-11" : args[0]; 

		      System.out.print(input + " Parses as "); 
		      Date t;
		      try {
		         t = ft.parse(input); 
		         System.out.println(t); 
		      }catch (ParseException e) { 
		         System.out.println("Unparseable using " + ft); 
		      }
		   }
	   public class CollectionsDemo {

		   public static void main(String[] args) {
		      // ArrayList 
		      List a1 = new ArrayList();
		      a1.add("Zara");
		      a1.add("Mahnaz");
		      a1.add("Ayan");
		      System.out.println(" ArrayList Elements");
		      System.out.print("\t" + a1);

		      // LinkedList
		      List l1 = new LinkedList();
		      l1.add("Zara");
		      l1.add("Mahnaz");
		      l1.add("Ayan");
		      System.out.println();
		      System.out.println(" LinkedList Elements");
		      System.out.print("\t" + l1);

		      // HashSet
		      Set s1 = new HashSet(); 
		      s1.add("Zara");
		      s1.add("Mahnaz");
		      s1.add("Ayan");
		      System.out.println();
		      System.out.println(" Set Elements");
		      System.out.print("\t" + s1);

		      // HashMap
		      Map m1 = new HashMap(); 
		      m1.put("Zara", "8");
		      m1.put("Mahnaz", "31");
		      m1.put("Ayan", "12");
		      m1.put("Daisy", "14");
		      System.out.println();
		      System.out.println(" Map Elements");
		      System.out.print("\t" + m1);
		   }
//		   ArrayList Elements
//			[Zara, Mahnaz, Ayan]
//		 LinkedList Elements
//			[Zara, Mahnaz, Ayan]
//		 Set Elements
//			[Ayan, Zara, Mahnaz]
//		 Map Elements
//			{Daisy = 14, Ayan = 12, Zara = 8, Mahnaz = 31}
		   public static void main(String[] args) {
			      List a1 = new ArrayList();
			      a1.add("Zara");
			      a1.add("Mahnaz");
			      a1.add("Ayan");      
			      System.out.println(" ArrayList Elements");
			      System.out.print("\t" + a1);

			      List l1 = new LinkedList();
			      l1.add("Zara");
			      l1.add("Mahnaz");
			      l1.add("Ayan");
			      System.out.println();
			      System.out.println(" LinkedList Elements");
			      System.out.print("\t" + l1);
			   }
//		   ArrayList Elements
//	        [Zara, Mahnaz, Ayan]
//	 LinkedList Elements
//	        [Zara, Mahnaz, Ayan]
		   public static void main(String[] args) {
			      Map m1 = new HashMap(); 
			      m1.put("Zara", "8");
			      m1.put("Mahnaz", "31");
			      m1.put("Ayan", "12");
			      m1.put("Daisy", "14");

			      System.out.println();
			      System.out.println(" Map Elements");
			      System.out.print("\t" + m1);
			   }
//		   Map Elements
//			{Daisy = 14, Ayan = 12, Zara = 8, Mahnaz = 31}
		   public static void main(String args[]) {
			      // Create a hash map
			      HashMap hm = new HashMap();

			      // Put elements to the map
			      hm.put("Zara", new Double(3434.34));
			      hm.put("Mahnaz", new Double(123.22));
			      hm.put("Ayan", new Double(1378.00));
			      hm.put("Daisy", new Double(99.22));
			      hm.put("Qadir", new Double(-19.08));
			      
			      // Get a set of the entries
			      Set set = hm.entrySet();
			      
			      // Get an iterator
			      Iterator i = set.iterator();
			     
			      // Display elements 
			      while(i.hasNext()) {
			         Map.Entry me = (Map.Entry)i.next();
			         System.out.print(me.getKey() + ": ");
			         System.out.println(me.getValue());
			      }
			      System.out.println();
			     
			      // Deposit 1000 into Zara's account
			      double balance = ((Double)hm.get("Zara")).doubleValue();
			      hm.put("Zara", new Double(balance + 1000));
			      System.out.println("Zara's new balance: " + hm.get("Zara"));
			   }
//		   Daisy: 99.22
//		   Ayan: 1378.0
//		   Zara: 3434.34
//		   Qadir: -19.08
//		   Mahnaz: 123.22
//
//		   Zara's new balance: 4434.34

		   public static void main(String args[]) {
		      // Create a hash map
		      TreeMap tm = new TreeMap();
		      
		      // Put elements to the map
		      tm.put("Zara", new Double(3434.34));
		      tm.put("Mahnaz", new Double(123.22));
		      tm.put("Ayan", new Double(1378.00));
		      tm.put("Daisy", new Double(99.22));
		      tm.put("Qadir", new Double(-19.08));
		      
		      // Get a set of the entries
		      Set set = tm.entrySet();
		      
		      // Get an iterator
		      Iterator i = set.iterator();
		      
		      // Display elements
		      while(i.hasNext()) {
		         Map.Entry me = (Map.Entry)i.next();
		         System.out.print(me.getKey() + ": ");
		         System.out.println(me.getValue());
		      }
		      System.out.println();
		      
		      // Deposit 1000 into Zara's account
		      double balance = ((Double)tm.get("Zara")).doubleValue();
		      tm.put("Zara", new Double(balance + 1000));
		      System.out.println("Zara's new balance: " + tm.get("Zara"));
		   }
//		   Ayan: 1378.0
//		   Daisy: 99.22
//		   Mahnaz: 123.22
//		   Qadir: -19.08
//		   Zara: 3434.34
//
//		   Zara's new balance: 4434.34
		   public static void main(String args[]) {
			      Enumeration days;
			      Vector dayNames = new Vector();
			      
			      dayNames.add("Sunday");
			      dayNames.add("Monday");
			      dayNames.add("Tuesday");
			      dayNames.add("Wednesday");
			      dayNames.add("Thursday");
			      dayNames.add("Friday");
			      dayNames.add("Saturday");
			      days = dayNames.elements();
			      
			      while (days.hasMoreElements()) {
			         System.out.println(days.nextElement()); 
			      }
			   }
//		   Sunday
//		   Monday
//		   Tuesday
//		   Wednesday
//		   Thursday
//		   Friday
//		   Saturday
		   public static void main(String args[]) {
			      // create an array list
			      ArrayList al = new ArrayList();
			      System.out.println("Initial size of al: " + al.size());

			      // add elements to the array list
			      al.add("C");
			      al.add("A");
			      al.add("E");
			      al.add("B");
			      al.add("D");
			      al.add("F");
			      al.add(1, "A2");
			      System.out.println("Size of al after additions: " + al.size());

			      // display the array list
			      System.out.println("Contents of al: " + al);

			      // Remove elements from the array list
			      al.remove("F");
			      al.remove(2);
			      System.out.println("Size of al after deletions: " + al.size());
			      System.out.println("Contents of al: " + al);
			   }
//		   Initial size of al: 0
//		   Size of al after additions: 7
//		   Contents of al: [C, A2, A, E, B, D, F]
//		   Size of al after deletions: 5
//		   Contents of al: [C, A2, E, B, D]
		   public static void main(String args[]) {
			   
			      // Create a hash map
			      HashMap hm = new HashMap();
			      
			      // Put elements to the map
			      hm.put("Zara", new Double(3434.34));
			      hm.put("Mahnaz", new Double(123.22));
			      hm.put("Ayan", new Double(1378.00));
			      hm.put("Daisy", new Double(99.22));
			      hm.put("Qadir", new Double(-19.08));
			      
			      // Get a set of the entries
			      Set set = hm.entrySet();
			      
			      // Get an iterator
			      Iterator i = set.iterator();
			      
			      // Display elements
			      while(i.hasNext()) {
			         Map.Entry me = (Map.Entry)i.next();
			         System.out.print(me.getKey() + ": ");
			         System.out.println(me.getValue());
			      }
			      System.out.println();
			      
			      // Deposit 1000 into Zara's account
			      double balance = ((Double)hm.get("Zara")).doubleValue();
			      hm.put("Zara", new Double(balance + 1000));
			      System.out.println("Zara's new balance: " + hm.get("Zara"));
			   }
//		   Daisy: 99.22
//		   Ayan: 1378.0
//		   Zara: 3434.34
//		   Qadir: -19.08
//		   Mahnaz: 123.22
//
//		   Zara's new balance: 4434.34
		    public static void main(String[] args) throws ParseException {
		        String date_s = "2011-01-18 00:00:00.0";

		        // *** note that it's "yyyy-MM-dd hh:mm:ss" not "yyyy-mm-dd hh:mm:ss"  
		        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		        Date date = dt.parse(date_s);

		        // *** same for the format String below
		        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		        System.out.println(dt1.format(date));
		    }
		    String date = "2011-01-18 00:00:00.0";
		    TemporalAccessor temporal = DateTimeFormatter
		        .ofPattern("yyyy-MM-dd HH:mm:ss.S")
		        .parse(date); // use parse(date, LocalDateTime::from) to get LocalDateTime
		    String output = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(temporal);
}
	   