import java.lang.reflect.Array;
import java.util.*;
public class FirstExample {

	public static void main (String[] args) {
		System.out.println("Enter values for average: ");
		Scanner scanner = new Scanner(System.in);
		String values = scanner.nextLine();
		String[] valueArray = values.split(",");
		float sum = 0;
		for (int i=0; i<valueArray.length; i++) {
			sum += Float.parseFloat(valueArray[i]);
		}
		System.out.println("Your sum is "+ sum);
	}
	
}
